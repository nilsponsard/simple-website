import { copySync, ensureDirSync, existsSync } from "https://deno.land/std@0.105.0/fs/mod.ts";

const template = Deno.readTextFileSync("src/website/template.html");

const contents = new Map() as Map<string, { style: string | undefined, body: string; }>;

const nav = [] as Array<{ title: string, name: string; }>;

export async function GeneratePage(path: string) {
    console.log("--> " + path);
    try {
        const settings = JSON.parse(Deno.readTextFileSync(path + "/settings.json"));

        const name = path.split("/").slice(-1).join("");


        if (settings.generator && settings.generator !== "") {

            const c = Deno.run({
                cmd: ["sh", "-c", `cd ${path} ; chmod +x ./${settings.generator} ; ./${settings.generator}`],
                stderr: "piped",
                stdout: "piped"
            });
            const stdOut = await c.output();
            const stdErr = await c.stderrOutput();
            console.log(new TextDecoder().decode(stdOut));
            if (stdErr.length > 0)
                console.error(new TextDecoder().decode(stdErr));
        }
        if (settings.output) {
            const output = Deno.readTextFileSync(path + "/" + settings.output);
            contents.set(name, { body: output, style: settings.style });
        }
        if (settings.nav && settings.nav.length > 0)
            nav.push({ title: settings.nav, name });

    } catch (e) {
        console.error(e);
    }
}


export function Render() {
    ensureDirSync("public/assets");
    ensureDirSync("public/style");


    copySync("src/website/style.css", "public/style/style.css", {
        overwrite: true
    });


    contents.forEach((value, key) => {

        if (existsSync("src/website/pages/" + key + "/assets")) {
            copySync("src/website/pages/" + key + "/assets", "public/assets/" + key, {
                overwrite: true
            });
        }

        let navHtml = "";
        for (let i = 0; i < nav.length; ++i) {
            const n = nav[i];
            navHtml += `<a href="${n.name == key ? "#top" : n.name + ".html"}" class="${n.name == key ? "active" : ""}">${n.title}</a>`;
        }

        let page = template.replaceAll("{{content}}", value.body).replaceAll("{{nav}}", navHtml);

        if (value.style) {
            Deno.copyFileSync("src/website/pages/" + key + "/" + value.style, "public/style/" + key + ".css");
            page = page.replaceAll("{{style}}", `<link rel="stylesheet" href="style/${key}.css">`);
        } else {
            page = page.replaceAll("{{style}}", ``);
        }

        Deno.writeTextFileSync("public/" + key + ".html", page);

    });
}