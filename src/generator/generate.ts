
import { GeneratePage, Render } from "./pages.ts";


let startRender = false;
const waitGroup = [] as Array<number>;
function askRender() {
    if (startRender && waitGroup.length === 0)
        Render();
}


const pagesFolder = Deno.readDirSync("src/website/pages");



for (const f of pagesFolder) {
    if (f.isDirectory) {
        waitGroup.push(1);
        GeneratePage("src/website/pages/" + f.name).then(() => {
            waitGroup.pop();
            askRender();
        });
    }
}

startRender = true;



