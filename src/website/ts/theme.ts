let buttons = document.querySelectorAll(".theme-toggle")
let body = document.body

function toggleTheme(){
    body.classList.toggle("light")
}

buttons.forEach((value)=>{
    value.addEventListener("click", toggleTheme)
})  