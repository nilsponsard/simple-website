
const projects = JSON.parse(Deno.readTextFileSync("projects.json")) as {
    [language: string]: {
        [project: string]: {
            "logo": string | undefined,
            "description": string,
            "source": string,
            "launch": string | undefined;
        };
    };
};



function selectSectionColor(language: string): string {
    let color = "#5c6bc0";

    switch (language) {
        case "TypeScript / JavaScript":
            color = "#00b6b6";
            break;
        case "Rust":
            color = "#f76200";
            break;
        case "C++":
            color = "#5c6bc0";
            break;

        case "Python":
            color = "#ffa300";
            break;
        case "Visual Studio Code extensions":
            color = "#23a7f2";
            break;
        case "Arduino / Gamebunio":
            color = "#19989d";
            break;
    }


    return color;
}


const templateProject = Deno.readTextFileSync("template.html");
const templateSection = Deno.readTextFileSync("languageSection.html");

let outHtml = "";


for (const language in projects) {

    let sectionProjectsHtml = "";

    for (const name in projects[language]) {
        let pHtml = templateProject;

        const project = projects[language][name];

        pHtml = pHtml.replaceAll("{{logo}}", project.logo ? `<img class="logo" src="${project.logo}" alt="logo"/>` : "");
        pHtml = pHtml.replaceAll("{{launch}}", project.launch ? `<a href="${project.launch}"><span class="icon material-icons-outlined">launch</span> Launch</a>` : "");
        pHtml = pHtml.replaceAll("{{source}}", project.source ? `<a href="${project.source}"><span class="icon material-icons-outlined">source</span> Source</a>` : "");
        pHtml = pHtml.replaceAll("{{name}}", name);
        pHtml = pHtml.replaceAll("{{description}}", project.description);

        sectionProjectsHtml += pHtml;
    }

    outHtml += templateSection.replaceAll("{{title}}", language).replaceAll("{{projects}}", sectionProjectsHtml).replaceAll("{{accent}}", selectSectionColor(language));
}

Deno.writeTextFileSync("projects.html", outHtml);
