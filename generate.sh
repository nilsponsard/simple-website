#!/bin/sh

mkdir -p public/style

deno run -A --unstable src/generator/generate.ts

cd src/website/ts && npx tsc
